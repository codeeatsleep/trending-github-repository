package com.assignment.githubrepo.github.ui.user

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.android.example.github.util.EspressoTestUtil
import com.android.example.github.util.RecyclerViewMatcher
import com.android.example.github.util.ViewModelUtil
import com.assignment.githubrepo.R
import com.assignment.githubrepo.data.Resource
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.github.util.SingleFragmentActivity
import com.assignment.githubrepo.github.util.TaskExecutorWithIdlingResourceRule
import com.assignment.githubrepo.github.util.TestUtil
import com.assignment.githubrepo.github.util.mock
import com.assignment.githubrepo.ui.repo.UserListFragment
import com.assignment.githubrepo.ui.repo.UserListFragmentDirections
import com.assignment.githubrepo.ui.repo.UserRecyclerViewAdapter
import com.assignment.githubrepo.ui.repo.UserViewModel
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

/**
 * Created by Vikas Gupta on 2/2/20.
 */
@RunWith(AndroidJUnit4::class)
class UserRepoFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)

     var userListLiveData = MutableLiveData<Resource<List<User>>>()
    lateinit var viewModel: UserViewModel
    private lateinit var mockAdapter: UserRecyclerViewAdapter
    var testUserListFragment = TestUserRepoFragment()

    @Before
    fun init() {
        viewModel = Mockito.mock(UserViewModel::class.java)
        mockAdapter = Mockito.mock(UserRecyclerViewAdapter::class.java)
        Mockito.doNothing().`when`(viewModel).setForceUpdateFlag(
            ArgumentMatchers.anyBoolean()
        )
        Mockito.`when`(viewModel.userList).thenReturn(userListLiveData)
        testUserListFragment.viewModelFactory = ViewModelUtil.createFor(viewModel)
        activityRule.activity.setFragment(testUserListFragment)
        EspressoTestUtil.disableProgressBarAnimations(activityRule)
    }

    @Test
    fun testLoading() {
        userListLiveData.postValue(Resource.loading(null))
        Espresso.onView(ViewMatchers.withId(R.id.laoding))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.err_msg))
            .check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun testValueWhileLoading() {
        val user = TestUtil.createUser("yigit")
        val userList = mutableListOf<User>()
        userList.add(user)
        userListLiveData.postValue(Resource.loading(userList))
        Espresso.onView(ViewMatchers.withId(R.id.laoding))
            .check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))
        Espresso.onView(ViewMatchers.withId(R.id.tv_user_name)).check(
            ViewAssertions.matches(
                ViewMatchers.withText("yigit name")
            )
        )
    }

    @Test
    fun testLoaded() {
        val user = TestUtil.createUser("foo")
        val userList = mutableListOf<User>()
        userList.add(user)
        userListLiveData.postValue(Resource.success(userList))
        Espresso.onView(ViewMatchers.withId(R.id.laoding))
            .check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))
        Espresso.onView(ViewMatchers.withId(R.id.tv_user_name)).check(
            ViewAssertions.matches(
                ViewMatchers.withText("foo name")
            )
        )
        Espresso.onView(ViewMatchers.withId(R.id.tv_repo_name))
            .check(ViewAssertions.matches(ViewMatchers.withText("Vikas foo")))
    }

    @Test
    fun testError() {
        userListLiveData.postValue(Resource.error("foo", null))
        Espresso.onView(ViewMatchers.withId(R.id.laoding))
            .check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))
        Espresso.onView(ViewMatchers.withId(R.id.err_msg))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

    @Test
    fun testItems() {
        setUserRepos()
        Espresso.onView(listMatcher().atPosition(0))
            .check(ViewAssertions.matches(ViewMatchers.hasDescendant(ViewMatchers.withText("foo name"))))
        Espresso.onView(listMatcher().atPosition(1))
            .check(ViewAssertions.matches(ViewMatchers.hasDescendant(ViewMatchers.withText("bar name"))))
    }

    private fun listMatcher(): RecyclerViewMatcher {
        return RecyclerViewMatcher(R.id.list)
    }

    @Test
    fun testItemClick() {
        setUserRepos()
        Espresso.onView(ViewMatchers.withText("foo name")).perform(ViewActions.click())
        Mockito.verify(testUserListFragment.navController).navigate(
            ArgumentMatchers.eq(UserListFragmentDirections.listToDetail("foo name","github.com")),
            ArgumentMatchers.any(FragmentNavigator.Extras::class.java)
        )
    }


    private fun setUserRepos(vararg names: String) {
        val userList = mutableListOf<User>()
        userList.add(TestUtil.createUser("foo"))
        userList.add(TestUtil.createUser("bar"))
        userListLiveData.postValue(Resource.success(userList))
    }

    class TestUserRepoFragment : UserListFragment(){
        val navController = mock<NavController>()
        override fun navController() = navController
    }
}