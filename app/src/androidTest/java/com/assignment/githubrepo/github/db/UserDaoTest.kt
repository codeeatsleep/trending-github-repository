/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assignment.githubrepo.github.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.github.util.LiveDataTestUtil.getValue
import com.assignment.githubrepo.github.util.TestUtil
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class UserDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun insertAndLoad() {
        var userList : MutableList<User> = mutableListOf()
        userList.add(TestUtil.createUser("foo"))
        userList.add(TestUtil.createUser("bar"))
        db.userDao().insertAll(userList)
        val userListFromDb = getValue(db.userDao().getUserList())
        assertThat("Check Insert Data",userListFromDb.size == userList.size)
    }

    @Test
    fun getUserRepo(){
        var userList : MutableList<User> = mutableListOf()
        userList.add(TestUtil.createUser("foo"))
        userList.add(TestUtil.createUser("bar"))
        db.userDao().insertAll(userList)
        val loaded = getValue(db.userDao().getRepoDetail(userList[1].username))
        assertThat(loaded.username, `is`("bar"))
        assertThat(loaded.repo?.name, `is`("Vikas bar"))
    }
}
