package com.assignment.githubrepo.github.ui.repodetail

import android.graphics.Bitmap
import android.util.LruCache
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.assignment.githubrepo.data.Resource
import com.assignment.githubrepo.data.UserRepository
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.github.util.TestUtil
import com.assignment.githubrepo.github.util.mock
import com.assignment.githubrepo.ui.repodetail.RepoDetailViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.reset

/**
 * Created by Vikas Gupta on 26/1/20.
 */
class RepoDetailViewModelTest {
        @Rule
        @JvmField
        val instantExecutorRule = InstantTaskExecutorRule()
        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()

        // Use 1/8th of the available memory for this memory cache.
        val cacheSize = maxMemory / 8

        val memoryCache =object : LruCache<String, Bitmap>(cacheSize) {

            override fun sizeOf(key: String, bitmap: Bitmap): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.byteCount / 1024
            }
        }

        private val userRepository = Mockito.mock(UserRepository::class.java)
        private val repoViewModel = RepoDetailViewModel(userRepository,memoryCache)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

        @Test
        fun testCallRepo() {
            val foo = MutableLiveData<User>()
            runBlocking {
                launch(Dispatchers.Main){
                    Mockito.`when`(userRepository.getRepoDetail("foo")).thenReturn(foo)
                }

            }
            repoViewModel.getRepoDetail("foo")
            runBlocking {
            Mockito.verify(userRepository).getRepoDetail("foo")
            }
            reset(userRepository)
            repoViewModel.getRepoDetail("foo")
            runBlocking {
                launch(Dispatchers.Main){
                    Mockito.verify(userRepository,never()).getRepoDetail("foo")
                }

            }
        }

        @Test
        fun sendResultToUI() {
            val foo = MutableLiveData<User>()
            val bar = MutableLiveData<User>()
            runBlocking {
                launch (Dispatchers.Main){
                    Mockito.`when`(userRepository.getRepoDetail("foo")).thenReturn(foo)
                }

            }
            val observer = mock<Observer<User>>()
            repoViewModel.userDetail.observeForever(observer)
            repoViewModel.getRepoDetail("foo")
            Mockito.verify(observer, never()).onChanged(Mockito.any())
            val fooUser = TestUtil.createUser("foo")
            foo.value = fooUser
            Mockito.verify(observer).onChanged(fooUser)
            foo.value = fooUser
            Mockito.verifyNoMoreInteractions(observer)
            reset(observer)
            runBlocking {
                launch (Dispatchers.Main){
                    Mockito.`when`(userRepository.getRepoDetail("bar")).thenReturn(bar)
                }

            }
            val barUser = TestUtil.createUser("bar")
            repoViewModel.getRepoDetail("bar")
            Mockito.verify(observer, Mockito.never()).onChanged(Mockito.any())
            bar.value = barUser
            Mockito.verify(observer).onChanged(barUser)
        }

        @Test
        fun dontRefreshOnSameData() {
            val foo = MutableLiveData<User>()
            runBlocking {
                launch (Dispatchers.Main){
                    Mockito.`when`(userRepository.getRepoDetail("foo")).thenReturn(foo)
                }

            }
            val observer = mock<Observer<User>>()
            repoViewModel.userDetail.observeForever(observer)
            repoViewModel.getRepoDetail("foo")
            Mockito.verify(observer, never()).onChanged(Mockito.any())
            val fooUser = TestUtil.createUser("foo")
            foo.value = fooUser
            Mockito.verify(observer).onChanged(fooUser)
            foo.value = fooUser
            Mockito.verifyNoMoreInteractions(observer)
        }


    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }
}