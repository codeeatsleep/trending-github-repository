package com.assignment.githubrepo.github.ui.user

import android.graphics.Bitmap
import android.util.LruCache
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.assignment.githubrepo.api.GithubService
import com.assignment.githubrepo.data.Resource
import com.assignment.githubrepo.data.UserRepository
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.db.UserRepoDao
import com.assignment.githubrepo.github.util.TestUtil
import com.assignment.githubrepo.github.util.mock
import com.assignment.githubrepo.ui.repo.UserViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.Mockito.*
import javax.inject.Inject

/**
 * Created by Vikas Gupta on 26/1/20.
 */
@RunWith(JUnit4::class)
class UserViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()

    // Use 1/8th of the available memory for this memory cache.
    val cacheSize = maxMemory / 8

    val memoryCache =object : LruCache<String, Bitmap>(cacheSize) {

        override fun sizeOf(key: String, bitmap: Bitmap): Int {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return bitmap.byteCount / 1024
        }
    }

    private val userRepository = mock(UserRepository::class.java)
    private val userViewModel = UserViewModel(userRepository,memoryCache)


    @Test
    fun testCallRepo() {
        val foo = MutableLiveData<Resource<List<User>>>()
        val bar = MutableLiveData<Resource<List<User>>>()
        Mockito.`when`(userRepository.getUserList(false)).thenReturn(foo)
        Mockito.`when`(userRepository.getUserList(true)).thenReturn(bar)
        val observer = mock<Observer<Resource<List<User>>>>()
        userViewModel.userList.observeForever(observer)
        userViewModel.setForceUpdateFlag(false)
        verify(userRepository).getUserList(false)

        userViewModel.setForceUpdateFlag(true)
        verify(userRepository).getUserList(true)
    }

    @Test
    fun sendResultToUI() {
        val foo = MutableLiveData<Resource<List<User>>>()
        val bar = MutableLiveData<Resource<List<User>>>()
        Mockito.`when`(userRepository.getUserList(false)).thenReturn(foo)
        Mockito.`when`(userRepository.getUserList(true)).thenReturn(bar)
        val observer = mock<Observer<Resource<List<User>>>>()
        userViewModel.userList.observeForever(observer)
        userViewModel.setForceUpdateFlag(false)
        Mockito.verify(observer, Mockito.never()).onChanged(Mockito.any())
        val fooUser = TestUtil.createUser("foo")
        val list = mutableListOf<User>()
        list.add(fooUser)
        val fooValue = Resource.success(list)
        foo.value = fooValue
        Mockito.verify(observer).onChanged(fooValue)
        reset(observer)
        val barUser = TestUtil.createUser("bar")
        val list2 = mutableListOf<User>()
        list2.add(barUser)
        val barValue = Resource.success(list2)
        bar.value = barValue
        userViewModel.setForceUpdateFlag(true)
        Mockito.verify(observer).onChanged(barValue)
    }

    @Test
    fun dontRefreshOnSameData() {
        val observer = mock<Observer<Resource<List<User>>>>()
        userViewModel.userList.observeForever(observer)
        Mockito.verifyNoMoreInteractions(observer)
        val foo = MutableLiveData<Resource<List<User>>>()
        Mockito.`when`(userRepository.getUserList(false)).thenReturn(foo)
        userViewModel.setForceUpdateFlag(false)
        val fooUser = TestUtil.createUser("foo")
        val list = mutableListOf<User>()
        list.add(fooUser)
        val fooValue = Resource.success(list)
        foo.value = fooValue
        verify(observer).onChanged(fooValue)
        foo.value = fooValue
        verifyNoMoreInteractions(observer)
    }
}