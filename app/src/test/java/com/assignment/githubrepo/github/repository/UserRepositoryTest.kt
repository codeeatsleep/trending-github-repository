package com.assignment.githubrepo.github.repository

import android.graphics.Bitmap
import android.util.LruCache
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import org.hamcrest.MatcherAssert.assertThat
import com.assignment.githubrepo.api.GithubService
import com.assignment.githubrepo.data.Resource
import com.assignment.githubrepo.data.UserRepository
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.db.UserRepoDao
import com.assignment.githubrepo.github.util.TestUtil
import com.assignment.githubrepo.github.util.mock
import com.assignment.githubrepo.github.util.toDeferred
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import java.io.ByteArrayOutputStream
import javax.inject.Inject


/**
 * Created by Vikas Gupta on 26/1/20.
 */
class UserRepositoryTest {
    private val userDao = Mockito.mock(UserRepoDao::class.java)
    private val githubService = Mockito.mock(GithubService::class.java)
    var w: Int = 200
    var h:Int = 200
    var conf = Bitmap.Config.ARGB_8888
    var dummyBitmap = Bitmap.createBitmap(w, h, conf)

    val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()

    // Use 1/8th of the available memory for this memory cache.
    val cacheSize = maxMemory / 8

    val memoryCache =object : LruCache<String, Bitmap>(cacheSize) {

        override fun sizeOf(key: String, bitmap: Bitmap): Int {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return bitmap.byteCount / 1024
        }
    }
    private val repo = UserRepository(userDao, githubService,memoryCache)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadRepoDetail() {
        runBlocking {
        repo.getRepoDetail("Vikas")
        }
        Mockito.verify(userDao).getRepoDetail("Vikas")
    }

    @Test
    fun goToNetwork() {
        val apiData = mutableListOf<User>()
        val dbData = MutableLiveData<List<User>>()
        val user = TestUtil.createUser("foo")
        val list =mutableListOf<User>()
        list.add(user)
        dbData.value = list
        Mockito.`when`(userDao!!.getUserList()).thenReturn(dbData)
        Mockito.`when`(githubService.getUserList()).thenReturn(apiData.toDeferred())
        val observer = mock<Observer<Resource<List<User>>>>()
        runBlocking {
        repo.getUserList(true).observeForever(observer)
        }
        Mockito.verify(observer).onChanged(Resource.success(list))
        Mockito.verify(githubService).getUserList()
    }

    @Test
    fun dontGoToNetwork() {
        val dbData = MutableLiveData<List<User>>()
        val user = TestUtil.createUser("foo")
        val list =mutableListOf<User>()
        list.add(user)
        dbData.value = list
        Mockito.`when`(userDao!!.getUserList()).thenReturn(dbData)
        val observer = mock<Observer<Resource<List<User>>>>()
        repo.getUserList(false).observeForever(observer)
        Mockito.verify(githubService, Mockito.never()).getUserList()
        Mockito.verify(observer).onChanged(Resource.success(list))
    }

    @Test
     fun downLoadImage() {
        val byteArray: ByteArray = byteArrayOf()
        Mockito.`when`(githubService!!.downloadFileWithDynamicUrlSync("foo")).thenReturn(ResponseBody.create(
            null,byteArray).toDeferred())
        runBlocking {
        repo.downloadImage("foo")
        }
        assertThat("search bitmap",memoryCache.get("foo") == null)

    }
}