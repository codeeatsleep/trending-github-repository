package com.assignment.githubrepo.data.remote

import androidx.room.Entity
import androidx.room.TypeConverters
import com.assignment.githubrepo.db.GithubTypeConverters
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(primaryKeys = ["username"])
@TypeConverters(GithubTypeConverters::class)
@JsonClass(generateAdapter = true)
data class User(

	@Json(name="repo")
	var repo: RepoDetail? = null,

	@Json(name="name")
	var name: String? = null,

	@Json(name="avatar")
	var avatar: String? = null,

	@Json(name="type")
	var type: String? = null,

	@Json(name="url")
	var url: String? = null,

	@Json(name="username")
	var username: String
)