package com.gojek.assignment.trendinggithubproject.data.local

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

/**
 * Created by Vikas Gupta on 19/01/2020.
 */
class SharedPref @Inject constructor(private val context: Context) {

    private val LAST_SYNC_TIME = "last_sync_time"

    fun getLastSyncTime(): Long{
       return getSharedPreference().getLong(LAST_SYNC_TIME,0L)
    }

    fun setLastSyncTime(time:Long) {
        getSharedPreference().edit().putLong(LAST_SYNC_TIME,time).apply()
    }

    private fun getSharedPreference(): SharedPreferences {
        return context.getSharedPreferences(LAST_SYNC_TIME, Context.MODE_PRIVATE)
    }
}