package com.assignment.githubrepo.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.LruCache
import androidx.lifecycle.LiveData
import com.assignment.githubrepo.api.GithubService
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.db.GithubDb
import com.assignment.githubrepo.db.UserRepoDao
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Vikas Gupta on 21/1/20.
 */
@OpenForTesting
@Singleton
class UserRepository  @Inject constructor(private val repoDao: UserRepoDao,
                                          private val githubService: GithubService,
                                          private val memoryCache: LruCache<String, Bitmap>
)  {
    fun getUserList(forceUpdate:Boolean): LiveData<Resource<List<User>>> {
        return  object:NetworkBoundResource<List<User>,List<User>>(){
            override fun saveCallResult(item: List<User>) {
                repoDao.insertAll(item)
            }

            override fun shouldFetch(data: List<User>?): Boolean {
                return forceUpdate /*|| (Calendar.getInstance().timeInMillis - prefs.getLastSyncTime() > 2*60*60*1000)*/
            }

            override fun loadFromDb(): LiveData<List<User>> {
                return repoDao.getUserList()
            }

            override fun createCall(): Deferred<List<User>> {
                return  githubService.getUserList()
            }

        }.asLiveData()
    }

    suspend fun downloadImage(url: String) {
        withContext(Dispatchers.IO){
            val response = githubService.downloadFileWithDynamicUrlSync(url)?.await()
            val byteArray = response?.bytes()
            byteArray?.let {
                val bitmap =
                    BitmapFactory.decodeByteArray(it,0,it.size)
                memoryCache.put(url, bitmap)
            }

        }

    }
    suspend fun getRepoDetail(username:String) = withContext(Dispatchers.Default){
        repoDao.getRepoDetail(username)
    }

}