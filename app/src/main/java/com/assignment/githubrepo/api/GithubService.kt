/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assignment.githubrepo.api

import com.assignment.githubrepo.data.remote.User
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url


/**
 * REST API access points
 */
interface GithubService {
    @GET("/developers?language=java&since=weekly")
    fun getUserList(): Deferred<List<User>>
    @GET
    fun downloadFileWithDynamicUrlSync(@Url fileUrl: String?): Deferred<ResponseBody?>?
}
