/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assignment.githubrepo.db

import androidx.room.TypeConverter
import com.assignment.githubrepo.data.remote.RepoDetail
import timber.log.Timber

object GithubTypeConverters {
    @TypeConverter
    @JvmStatic
    fun stringToRepoDetailObject(data: String): RepoDetail {
        val repoItems = data.split(",")
        return RepoDetail(repoItems[0],repoItems[2],repoItems[1])
    }

    @TypeConverter
    @JvmStatic
    fun RepoDetailObjectToString(repoDetail: RepoDetail): String {
        return "${repoDetail.name},${repoDetail.url},{${repoDetail.description}}"
    }
}
