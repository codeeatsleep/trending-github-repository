package com.assignment.githubrepo.ui.repo

import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.assignment.githubrepo.R
import com.assignment.githubrepo.data.OpenForTesting
import com.assignment.githubrepo.data.remote.User

import kotlinx.android.synthetic.main.fragment_repo.view.*


/**
 */
@OpenForTesting
class UserRecyclerViewAdapter(
    private val userViewModel: UserViewModel,
    private val callback: ((User, ImageView) -> Unit)
) : RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder>() {
    var userList: List<User>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_repo, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = userList!![position]

         with(holder.mView) {
             holder.repoName.text = item.repo?.name ?: "NA"
             holder.userName.text = item.name
             item.avatar?.let {
                 userViewModel.loadBitmap(it,holder.userPhoto)
                 holder.userPhoto.transitionName = item.username
             }

             setOnClickListener{
                 callback.invoke(item,thumbnail)
             }

         }
    }

    override fun getItemCount(): Int = userList?.size ?: 0

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val userName: TextView = mView.tv_user_name
        val repoName: TextView = mView.tv_repo_name
        val userPhoto: ImageView = mView.thumbnail
    }
}
