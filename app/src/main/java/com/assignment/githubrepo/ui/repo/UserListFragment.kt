package com.assignment.githubrepo.ui.repo

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.assignment.githubrepo.R
import com.assignment.githubrepo.data.OpenForTesting
import com.assignment.githubrepo.data.Status
import com.assignment.githubrepo.di.Injectable
import kotlinx.android.synthetic.main.fragment_repo_list.*
import kotlinx.android.synthetic.main.fragment_repo_list.view.*
import javax.inject.Inject

/**
 * A fragment representing a list of Items.
 */
@OpenForTesting
class UserListFragment : Fragment(),Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var adapter : UserRecyclerViewAdapter
    private var handler = Handler(Looper.getMainLooper())

      private val userViewModel : UserViewModel by viewModels {
        viewModelFactory
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_repo_list, container, false)
        sharedElementReturnTransition =  TransitionInflater.from(context).inflateTransition(R.transition.move)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = UserRecyclerViewAdapter(
            userViewModel
        ) { repo, imageView ->
            val extras = FragmentNavigatorExtras(
                imageView to repo.username
            )

            findNavController().navigate(
                UserListFragmentDirections.listToDetail(
                    repo.username,
                    repo.avatar
                ), extras
            )
        }
       postponeEnterTransition()
        handler.postDelayed(100){
            startPostponedEnterTransition()
        }

        view.list.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel.setForceUpdateFlag(true)
        userViewModel.userList.observe(viewLifecycleOwner, Observer {
            when(it.status){
                 Status.SUCCESS -> {
                     laoding.visibility = View.GONE

                     err_msg.visibility = View.GONE
                     adapter.userList = it.data
                     adapter.notifyDataSetChanged()
                     list.visibility =View.VISIBLE
                 }
                Status.LOADING -> {
                    laoding.visibility = View.VISIBLE
                    list.visibility =View.GONE
                    err_msg.visibility = View.GONE
                }
                Status.ERROR -> {
                    laoding.visibility = View.GONE
                    list.visibility =View.GONE
                    err_msg.setText(it.message)
                    err_msg.visibility = View.VISIBLE

                }
            }
        })
    }
    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()
}
