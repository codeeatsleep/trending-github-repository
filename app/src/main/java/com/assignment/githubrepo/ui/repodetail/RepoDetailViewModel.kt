package com.assignment.githubrepo.ui.repodetail

import android.graphics.Bitmap
import android.util.LruCache
import android.widget.ImageView
import androidx.lifecycle.*
import com.assignment.githubrepo.R
import com.assignment.githubrepo.data.UserRepository
import com.assignment.githubrepo.data.remote.User
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Vikas Gupta on 25/1/20.
 */
class RepoDetailViewModel @Inject constructor(val userRepo: UserRepository, private val memoryCache: LruCache<String, Bitmap>) : ViewModel() {

    private val _repoDetail = MediatorLiveData<User>()
    val userDetail : LiveData<User>
        get() = _repoDetail

    private var oldUserName:String?= null

    fun getRepoDetail(userName:String) {
        if(oldUserName == userName)
            return
        oldUserName = userName
        viewModelScope.launch {
            if(_repoDetail.hasObservers())_repoDetail.removeSource(_repoDetail)
            _repoDetail.addSource(Transformations.distinctUntilChanged(userRepo.getRepoDetail(userName))){
                repo ->
                _repoDetail.value = repo
            }
        }
    }

    fun loadBitmap(url: String, imageView: ImageView) {
         memoryCache.get(url)?.also {
            imageView.setImageBitmap(it)
        } ?: run {
            imageView.setImageResource(R.drawable.ic_launcher_background)
            viewModelScope.launch {
                val imageDownLoad =  async {  userRepo.downloadImage(url) }
                imageDownLoad.await()
                memoryCache.get(url)?.also {
                    imageView.setImageBitmap(it)
                }
            }
            null
        }
    }
}