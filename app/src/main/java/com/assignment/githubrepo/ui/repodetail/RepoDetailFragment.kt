package com.assignment.githubrepo.ui.repodetail


import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater

import com.assignment.githubrepo.R
import com.assignment.githubrepo.di.Injectable
import kotlinx.android.synthetic.main.fragment_repo.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Use the [RepoDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RepoDetailFragment : Fragment(),Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var handler = Handler(Looper.getMainLooper())


    private val repoDetailViewModel : RepoDetailViewModel by viewModels {
        viewModelFactory
    }
    private val params by navArgs<RepoDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_repo_detail, container, false)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.move)
        handler.postDelayed(100){
            startPostponedEnterTransition()
        }
        postponeEnterTransition()
        return view
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        repoDetailViewModel.getRepoDetail(params.username)
        repoDetailViewModel.userDetail.observe(viewLifecycleOwner, Observer { repo ->
            repo.avatar?.let {
                repoDetailViewModel.loadBitmap(it,thumbnail)
                thumbnail.transitionName = repo.username
            }
            tv_user_name.text = repo.username
            tv_repo_name.text = repo.url

        })
    }
}
