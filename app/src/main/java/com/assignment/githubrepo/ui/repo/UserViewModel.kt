package com.assignment.githubrepo.ui.repo

import android.graphics.Bitmap
import android.util.LruCache
import android.widget.ImageView
import androidx.lifecycle.*
import com.assignment.githubrepo.R
import com.assignment.githubrepo.data.OpenForTesting
import com.assignment.githubrepo.data.Resource
import com.assignment.githubrepo.data.UserRepository
import com.assignment.githubrepo.data.remote.User
import com.assignment.githubrepo.ui.AbsentLiveData
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Vikas Gupta on 19/1/20.
 */
@OpenForTesting
class UserViewModel@Inject constructor(val userRepo: UserRepository, private val memoryCache: LruCache<String, Bitmap>) : ViewModel() {

    private val _forceUpdate = MutableLiveData<Boolean>()

     val userList : LiveData<Resource<List<User>>> = Transformations.switchMap(_forceUpdate) {
            forceUpdate ->
        if(forceUpdate == null){
            AbsentLiveData.create()
        } else {
             Transformations.distinctUntilChanged(userRepo.getUserList(forceUpdate))
        }
    }

    fun setForceUpdateFlag(flag:Boolean){
        if( _forceUpdate.value != flag){
            _forceUpdate.value = flag
        }
    }

    fun loadBitmap(url: String, imageView: ImageView) {
        memoryCache.get(url)?.also {
            imageView.setImageBitmap(it)
        } ?: run {
            imageView.setImageResource(R.drawable.ic_launcher_background)
            viewModelScope.launch {
               val imageDownLoad =  async {  userRepo.downloadImage(url) }
                imageDownLoad.await()
                memoryCache.get(url)?.also {
                    imageView.setImageBitmap(it)
                }
            }
            null
        }
    }

}