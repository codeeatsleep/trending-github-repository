# Trending GitHub Repository
problem statement version 1: 
1.fetch the trending git hub repo with given lang and from (weekly) 
2.on click git hub repo it will show the detail to next screen
3.transition between fragment by shared element(profile photo)

# Tech Stack(Library used)
Arch.- MVVM ViewModel to survive the state during orientation change 
LiveData for reactive bindings 
Room ORM - for Api caching (single source of truth)
LruCache - to cache image 
Coroutines for async tasks
Moshi for kotlin friendly json parsing
Mockito for mocking object

# TO DO

# pending task
1.write ui testcases 
2.correct failing test cases 
3.show all repo detail in ui

# image cache
1.disk cache to save api call 
2.store resize image in cache rather than with url

# performance
1.Diff Util to not refresh not visible and not changed data 
2.Pagination to take only limited amount of data if repo list range is too big(future)
